import { useState } from "react";

import toast, { Toaster } from "react-hot-toast";

import ReposBox from "./components/ReposBox";
import SearchBar from "./components/SearchBar";

import "./style.css";

const App = () => {
  const [repoList, setRepoList] = useState([]);

  const getRepo = (user) => {
    fetch(`https://api.github.com/repos/${user}`)
      .then((response) => response.json())
      .then((response) => {
        if (response.message) {
          toast.error(response.message);
        } else {
          setRepoList([...repoList, response]);
        }
      });
  };

  return (
    <div className="content">
      <Toaster />
      <header>
        <SearchBar getRepo={getRepo} />
      </header>
      <main>
        {repoList.length > 0 ? <ReposBox repoList={repoList} /> : <div></div>}
      </main>
    </div>
  );
};

export default App;
