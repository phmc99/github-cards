import styled from "styled-components";

export const SearchBox = styled.div`
  box-sizing: border-box;
  background-color: #f3f3f3;
  padding: 5px;
  border-radius: 5px;
  width: 300px;
  display: flex;
  justify-content: space-around;
  box-shadow: 2px 2px 5px gray;
`;

export const Input = styled.input`
  border: 0;
  background-color: lightgray;
  padding: 10px;
  border-radius: 5px;
  width: 180px;
`;

export const Button = styled.button`
  border: 0;
  background-color: lightgray;
  padding: 10px;
  border-radius: 5px;
  background-color: #3a6c93;
`;
