import { SearchBox, Button, Input } from "./style";
import { useState } from "react";
import toast from "react-hot-toast";

const SearchBar = ({ getRepo }) => {
  const [userInput, setUserInput] = useState("");

  const handleClick = () => {
    if (userInput.trim() !== "") {
      getRepo(userInput);
    } else {
      toast.error("Digite um repositorio valido");
    }
  };

  return (
    <SearchBox>
      <Input
        type="text"
        value={userInput}
        onChange={(event) => setUserInput(event.target.value)}
      />
      <Button onClick={handleClick}>Pesquisar</Button>
    </SearchBox>
  );
};

export default SearchBar;
