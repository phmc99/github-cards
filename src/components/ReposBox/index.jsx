import { Box, RepoList, RepoListItem } from "./style";

const ReposBox = ({ repoList }) => {
  return (
    <Box>
      <RepoList>
        {repoList.map((item, index) => (
          <RepoListItem key={index}>
            <img src={item.owner["avatar_url"]} alt="" />
            <div className="text-content">
              <h3>{item.name}</h3>
              <span>{item.description}</span>
            </div>
          </RepoListItem>
        ))}
      </RepoList>
    </Box>
  );
};

export default ReposBox;
