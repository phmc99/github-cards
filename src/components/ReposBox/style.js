import styled from "styled-components";

export const Box = styled.div`
  width: 300px;
  background-color: #f3f3f3;
  box-shadow: 2px 2px 5px gray;
`;

export const RepoList = styled.ul`
  padding: 0;
  list-style: none;
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 10px;
`;

export const RepoListItem = styled.li`
  border-radius: 5px;
  background: linear-gradient(
    90deg,
    rgba(2, 0, 36, 1) 0%,
    rgba(98, 148, 190, 1) 0%,
    rgba(58, 108, 147, 1) 100%
  );
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 120px;

  img {
    width: 80px;
    height: 80px;
    border-radius: 50%;
  }

  .text-content {
    width: 150px;

    h3 {
      margin: 0;
    }

    span {
      font-size: 12px;
      overflow: hidden;
    }
  }
`;
